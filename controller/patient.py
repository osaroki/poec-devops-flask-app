from flask import render_template


class PatientController():

    
    def create(self, model, patient):
        return model.create(patient)

    def read_all(self, model):
        return render_template('patients.html', obj=model.read_all())

    def read(self, model, numsec):
        return render_template('patient.html', obj=model.read(numsec))
    
    def read_shortcut(self, model, numsec):
        return render_template('patient.html', obj=model.read_shortcut(numsec))

    def update(self, model, patient):
        return model.update(patient)

    def delete(self, model, patient):
        return model.delete(patient.get('numsec'))

    def delete_shortcut(self, model, numsec):
        return model.delete_shortcut(numsec)

