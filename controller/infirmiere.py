from flask import render_template

class InfirmiereController():

    def create(self, model, infirmiere):
        return model.create(infirmiere)

    def read_all(self, model):
        return render_template('infirmieres.html', obj = model.read_all())
 
    def read(self, model, infirmiere):
        return render_template('infirmiere.html', obj = model.read(infirmiere)) 

    def read_shortcut(self, model, numpro):
        return render_template('infirmiere.html', obj = model.read_shortcut(numpro))

    def update(self, model, infirmiere):
        return model.update(infirmiere)

    def delete(self, model, infirmiere):
        return model.delete(infirmiere.get('numpro'))
    
    def delete_shortcut(self, model, numpro):
        return model.delete_shortcut(numpro)

    def error(self, model, numpro):
        return render_template('error.html', obj = model.read_patients(numpro))


        



