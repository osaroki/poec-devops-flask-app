from model.db import Connect

class InfirmiereModel:

    def __init__(self):
        self.conn = Connect.log()
    
    def create(self, infirmiere):
        self.conn.execute(
            f"""INSERT INTO adresse(numero, rue, cp, ville) values(
                '{str(infirmiere.get('numero'))}',
                '{str(infirmiere.get('rue'))}',
                '{int(infirmiere.get('cp'))}',
                '{str(infirmiere.get('ville'))}') """
        )
        self.conn.execute(
        f""" SELECT id FROM adresse AS a 
        WHERE 
            a.numero = '{str(infirmiere.get('numero'))}' AND 
            a.rue = '{str(infirmiere.get('rue'))}' AND 
            a.cp = '{str(infirmiere.get('cp'))}' AND 
            a.ville = '{str(infirmiere.get('ville'))}' 
        """)
        
        rows = self.conn.fetchall()
        adresse_id = rows[0][0]
               
        self.conn.execute(
            f"""INSERT INTO infirmiere(adresse_id,numeroProfessionnel, nom, prenom, telPro,telPerso) VALUES(
                {adresse_id},
                '{int(infirmiere.get('numeroProfessionnel'))}',
                '{str(infirmiere.get('nom'))}',
                '{str(infirmiere.get('prenom'))}',
                '{int(infirmiere.get('telPro'))}',
                '{int(infirmiere.get('telPerso'))}')"""
        )


    def read_all(self):
        self.conn.execute(
            f""" SELECT i.*, CONCAT(a.numero, ' ', a.rue, ' ', a.cp, ' ', a.ville) AS adresse, a.id AS adresse_id, a.numero AS numero, a.rue AS rue, a.cp AS cp, a.ville AS ville FROM infirmiere AS i INNER JOIN adresse AS a ON i.adresse_id = a.id """)
        affichage = self.conn.fetchall()
        return affichage



    def read(self, numpro):
        self.conn.execute(
            f""" SELECT i.*, CONCAT(a.numero, ' ', a.rue, ' ', a.cp, ' ', a.ville) AS adresse, a.id AS adresse_id, a.numero AS numero, a.rue AS rue, a.cp AS cp, a.ville AS ville FROM infirmiere AS i INNER JOIN adresse AS a ON i.adresse_id = a.id WHERE i.numeroProfessionnel = '{int(numpro.get('numpro'))}' """)
        affichage = self.conn.fetchall()
        return affichage

    def read_shortcut(self, numpro):
        self.conn.execute(
            f""" SELECT i.*, CONCAT(a.numero, ' ', a.rue, ' ', a.cp, ' ', a.ville) AS adresse, a.id AS adresse_id, a.numero AS numero, a.rue AS rue, a.cp AS cp, a.ville AS ville FROM infirmiere AS i INNER JOIN adresse AS a ON i.adresse_id = a.id WHERE i.numeroProfessionnel = {numpro} """)
        affichage = self.conn.fetchall()
        return affichage

    def update(self, infirmiere):
        self.conn.execute(
        f""" UPDATE adresse AS a SET a.numero = '{str(infirmiere.get('numero'))}', a.rue = '{str(infirmiere.get('rue'))}', a.cp = '{str(infirmiere.get('cp'))}', a.ville = '{str(infirmiere.get('ville'))}' WHERE a.id = '{int(infirmiere.get('adresse_id'))}'
        """)
        self.conn.execute(
        f""" UPDATE infirmiere AS i SET i.numeroProfessionnel = '{int(infirmiere.get('numeroProfessionnel'))}', i.nom = '{str(infirmiere.get('nom'))}', i.prenom = '{str(infirmiere.get('prenom'))}', i.telPro = '{int(infirmiere.get('telPro'))}', i.telPerso = '{int(infirmiere.get('telPerso'))}' WHERE i.id = '{int(infirmiere.get('id'))}'
        """)

    def delete(self, numpro):
        self.conn.execute(
        f""" DELETE FROM infirmiere WHERE numeroProfessionnel = '{numpro}'
        """)

    def delete_shortcut(self, numpro):
        self.conn.execute(
        f""" DELETE FROM infirmiere WHERE numeroProfessionnel = {numpro}
        """)

    def read_patients(self, numpro):
        self.conn.execute(
        f""" SELECT id FROM infirmiere WHERE  numeroProfessionnel = {numpro}
        """)
        rows = self.conn.fetchall()
        id = rows[0][0]
        self.conn.execute(
        f""" SELECT CONCAT(p.nom, ' ', p.prenom, ' ',p.numeroSecuriteSocial) AS patient, p.numeroSecuriteSocial AS numsec, {numpro} AS numpro FROM patient AS p WHERE  p.infirmiere_id = {id}
        """)
        rows = self.conn.fetchall()
        return rows

        
