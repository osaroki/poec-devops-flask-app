from model.db import Connect


class PatientModel:
    def __init__(self):
        self.conn = Connect.log()

    def create(self, patient):
        self.conn.execute(
        f""" INSERT INTO adresse(numero, rue, cp, ville) VALUES('{str(patient.get('numero'))}', '{str(patient.get('rue'))}', '{int(patient.get('cp'))}', '{str(patient.get('ville'))}') """)
        self.conn.execute(f""" SELECT id FROM adresse AS a WHERE a.numero = '{str(patient.get('numero'))}' AND a.rue = '{str(patient.get('rue'))}' AND a.cp = '{str(patient.get('cp'))}' AND a.ville = '{str(patient.get('ville'))}' """)
        rows = self.conn.fetchall()
        adresse = rows[0][0]
        self.conn.execute(f""" INSERT INTO patient(adresse_id, infirmiere_id, nom, prenom, dateDeNaissance, sexe, numeroSecuriteSocial) VALUES({adresse},'{int(patient.get('infirmiere_id'))}', '{str(patient.get('nom'))}', '{str(patient.get('prenom'))}', '{str(patient.get('naissance'))}', '{str(patient.get('sexe'))}', '{int(patient.get('numsec'))}') """)
    
    def read_all(self):
        self.conn.execute(
        """ SELECT p.*, i.numeroProfessionnel AS infirmiere,i.id AS id_infirmiere, CONCAT(i.prenom,' ',i.nom,' - ', i.numeroProfessionnel) AS nom_infirmiere, CONCAT(a.numero,' ', a.rue,' ', a.cp,' ', a.ville) AS adresse, a.id AS adresse_id, a.numero AS numero, a.rue AS rue, a.cp AS vp, a.ville AS ville FROM patient as p INNER JOIN infirmiere AS i ON p.infirmiere_id = i.id INNER JOIN adresse AS a ON p.adresse_id = a.id """)
        rows = self.conn.fetchall()
        return rows

    def read(self, numsec):
        self.conn.execute(
        f""" SELECT p.*, i.numeroProfessionnel AS infirmiere,i.id AS id_infirmiere, CONCAT(i.prenom,' ',i.nom,' - ', i.numeroProfessionnel) AS nom_infirmiere, CONCAT(a.numero,' ', a.rue,' ', a.cp,' ', a.ville) AS adresse, a.id AS adresse_id, a.numero AS numero, a.rue AS rue, a.cp AS vp, a.ville AS ville FROM patient as p INNER JOIN infirmiere AS i ON p.infirmiere_id = i.id INNER JOIN adresse AS a ON p.adresse_id = a.id WHERE p.numeroSecuriteSocial = '{int(numsec.get('numsec'))}' """)
        row = self.conn.fetchall()
        return row

    def read_shortcut(self, numsec):
        self.conn.execute(
        f""" SELECT p.*, i.numeroProfessionnel AS infirmiere,i.id AS id_infirmiere, CONCAT(i.prenom,' ',i.nom,' - ', i.numeroProfessionnel) AS nom_infirmiere, CONCAT(a.numero,' ', a.rue,' ', a.cp,' ', a.ville) AS adresse, a.id AS adresse_id, a.numero AS numero, a.rue AS rue, a.cp AS vp, a.ville AS ville FROM patient as p INNER JOIN infirmiere AS i ON p.infirmiere_id = i.id INNER JOIN adresse AS a ON p.adresse_id = a.id WHERE p.numeroSecuriteSocial = {numsec} """)
        row = self.conn.fetchall()
        return row

    def update(self, patient):
        self.conn.execute(f""" UPDATE adresse AS a SET a.numero = '{str(patient.get('numero'))}', a.rue = '{str(patient.get('rue'))}', a.cp = '{str(patient.get('cp'))}', a.ville = '{str(patient.get('ville'))}' WHERE a.id = '{int(patient.get('adresse_id'))}' """)
        self.conn.execute(f""" UPDATE patient AS p SET p.nom =  '{str(patient.get('nom'))}', p.prenom = '{str(patient.get('prenom'))}', p.dateDeNaissance = '{str(patient.get('naissance'))}', p.sexe = '{str(patient.get('sexe'))}', p.numeroSecuriteSocial = '{int(patient.get('numsec'))}' WHERE id = '{int(patient.get('id'))}' """)

    def delete(self, numsec):
        self.conn.execute(
        f""" DELETE FROM patient WHERE numeroSecuriteSocial = { numsec } """)

    def delete_shortcut(self, numsec):
        self.conn.execute(
        f""" DELETE FROM patient WHERE numeroSecuriteSocial = { numsec } """)
