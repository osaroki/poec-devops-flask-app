from abc import ABC

import mysql.connector

class Connect(ABC):
    
    conn = mysql.connector.connect(
        host="mysql",
        user="root",
        password="root",
        database="cabinet_medical",
        port="3306" ,
        auth_plugin='mysql_native_password',
    )

    conn.autocommit = True

    @staticmethod
    def log():

        try:
            cursor = Connect.conn.cursor(buffered=True)
            # You can specify here that you want to get a dict instead of a list
            return cursor
        except mysql.connector.Error as err:
            print(err)
