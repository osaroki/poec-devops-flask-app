# IMPORTS

import mysql.connector 

from flask import Flask, request
from flask import url_for, redirect

from controller.patient import PatientController
from model.patient import PatientModel

from controller.infirmiere import InfirmiereController
from model.infirmiere import InfirmiereModel


# INSTANCES

app = Flask(__name__)
if __name__ == "__main__":
   app.run(host ='0.0.0.0', port=5000)

patient_controller = PatientController()
patient_model = PatientModel()
infirmiere_model = InfirmiereModel()
infirmiere_controller = InfirmiereController()

@app.route('/')
def index():
    return redirect(url_for('patients'))

# CRUD PATIENT 


@app.route('/patients/')
def index_patients():
    return patient_controller.read_all(patient_model)

@app.route('/create_patient/', methods=['POST', 'GET'])
def create():
    form_data = request.form
    patient_controller.create(patient_model, form_data)
    return redirect(url_for('patients'))

@app.route('/read_patient/', methods=['POST','GET'])
def read():
    form_data = request.form
    return patient_controller.read(patient_model, form_data)

@app.route('/read_patient_shortcut/<int:numsec>')
def read_shortcut(numsec):
    return patient_controller.read_shortcut(patient_model, numsec)


@app.route('/update_patient/', methods=['POST', 'GET'])
def update():
    form_data = request.form
    patient_controller.update(patient_model, form_data)
    return redirect(url_for('patients'))


@app.route('/delete_patient/', methods=['POST','GET'])
def delete():
    form_data = request.form
    patient_controller.delete(patient_model, form_data)
    return redirect(url_for('patients'))


@app.route('/delete_patient_shortcut/<int:numsec>')
def delete_shortcut(numsec):
    patient_controller.delete_shortcut(patient_model, numsec)
    return redirect(url_for('patients'))
 
@app.route('/delete_patient_list/', methods=['POST', 'GET'])
def test():
    numsec = request.form['numsec']
    patient_controller.delete_shortcut(patient_model, numsec)
    return redirect(url_for('infirmieres'))


# CRUD INFIRMIERE

@app.route('/infirmieres/')
def index_infirmieres():
    return infirmiere_controller.read_all(infirmiere_model)


@app.route('/create_infirmiere/', methods=['POST', 'GET'])
def create_infirmiere():
    form_data = request.form
    infirmiere_controller.create(infirmiere_model, form_data)
    return redirect(url_for('infirmieres'))


@app.route('/read_infirmiere/', methods=['POST','GET'])
def read_infirmiere():
    form_data = request.form
    return infirmiere_controller.read(infirmiere_model, form_data)


@app.route('/read_infirmiere_shortcut/<int:numpro>')
def update_infirmiere_shortcut(numpro):
    return infirmiere_controller.read_shortcut(infirmiere_model, numpro)
 

@app.route('/update_infirmiere/', methods=['POST', 'GET'])
def update_infirmiere():
    form_data = request.form
    infirmiere_controller.update(infirmiere_model, form_data)
    return redirect(url_for('infirmieres'))


@app.route('/delete_infirmiere/', methods=['POST', 'GET'])
def delete_infirmiere():
    try:
        form_data = request.form
        infirmiere_controller.delete(infirmiere_model, form_data)
        return redirect(url_for('infirmieres'))
    except mysql.connector.IntegrityError:
        return infirmiere_controller.error(infirmiere_model, request.form['numpro'])

@app.route('/delete_infirmiere_shortcut/<int:numpro>')
def delete_infirmiere_shortcut(numpro):
    try:
        infirmiere_controller.delete_shortcut(infirmiere_model, numpro)       
        return redirect(url_for('infirmieres'))
    except mysql.connector.IntegrityError:
        return infirmiere_controller.error(infirmiere_model, numpro)
