## Project completed during a DevOps bootcamp @ M2i Formation

## Description

Develop a medical office management app in 3 days

## Stack [Given by the instructor]

1. Flask
2. Python
3. Bootstrap
4. MVC2
5. MySQL
6. Docker

## Installation

* git clone https://github.com/Osaroki/poec-devops-flask-app.git
* cd poec-devops-flask-app
* docker-compose up
* access on localhost:5000
